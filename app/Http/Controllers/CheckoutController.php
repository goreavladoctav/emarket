<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use Auth;
use App\Models\OrderItem;

class CheckoutController extends Controller
{
    public function checkout(Request $request) 
    {
        $total = 0;
        foreach($request->cart as $product) {
            $total += $product['price'] * $product['quantity'];
        }

        $order = new Order;
        $order->total = $total;
        $order->user_id = Auth::id();
        $order->save();
        
         foreach($request->cart as $product ) {
            $orderItem =  new OrderItem;
            $orderItem->product_id = $product['id'];
            $orderItem->quantity =  $product['quantity'];
            $orderItem->order_id = $order->id;
            $orderItem->save();
         }    

        return $order;
    }
}
