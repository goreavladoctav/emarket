<?php

namespace App\Http\Controllers;

use App\Models\FrontPage;
use Illuminate\Http\Request;

class FrontPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return redirect('/products');
        return view('frontpage.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FrontPage  $frontPage
     * @return \Illuminate\Http\Response
     */
    public function show(FrontPage $frontPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FrontPage  $frontPage
     * @return \Illuminate\Http\Response
     */
    public function edit(FrontPage $frontPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FrontPage  $frontPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FrontPage $frontPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FrontPage  $frontPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(FrontPage $frontPage)
    {
        //
    }
}
