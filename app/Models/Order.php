<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $with = ['items'];

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function products() 
    {
        return $this->hasManyThrough('App\Models\OrderItem', 'App\Models\Product');
    }
}
