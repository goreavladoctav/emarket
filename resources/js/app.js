/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
window.Vuex = require('vuex');

import Vue from 'vue';
import store from './store';
import VueRouter from 'vue-router'

Vue.use(VueRouter)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('add-product', require('./components/AddProduct.vue').default);
Vue.component('list-products', require('./components/ListProducts.vue').default);
Vue.component('edit-product', require('./components/EditProduct.vue').default);
Vue.component('add-category', require('./components/AddCategory.vue').default);
Vue.component('list-categories', require('./components/ListCategories.vue').default);
Vue.component('edit-category', require('./components/EditCategory.vue').default);
Vue.component('admin-page', require('./components/AdminPage.vue').default);
Vue.component('admin-list-orders', require('./components/ListOrders.vue').default);
Vue.component('admin-categories-links', require('./components/AdminCategoriesLinks.vue').default);
Vue.component('admin-products-links', require('./components/AdminOrdersLinks.vue').default);
Vue.component('admin-orders-links', require('./components/AdminProductsLinks.vue').default);


//frontpage component below 

Vue.component('front-list-products', require('./components/frontpage/ListProducts').default);
Vue.component('cart-component', require('./components/frontpage/CartComponent.vue').default);
Vue.component('list-orders', require('./components/frontpage/ListOrders').default);
Vue.component('front-page', require('./components/frontpage/FrontPage.vue').default);
Vue.component('front-page-product-details', require('./components/frontpage/ProductDetails.vue').default);
Vue.component('user-order-details', require('./components/frontpage/OrderDetails.vue').default);
Vue.component('cart-link', require('./components/frontpage/CartLink.vue').default);
Vue.component('order-link', require('./components/frontpage/OrderLink.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const routes = [
    {
        path: '/products/:id',
        component: require('./components/frontpage/ProductDetails.vue').default
    },

    {
        path: '/products', 
        component: require('./components/frontpage/ListProducts').default
    },

    {
        path: '/userorders',
        component: require('./components/frontpage/ListOrders').default
    },

    {
        path: '/myorders/:id',
        component: require('./components/frontpage/OrderDetails.vue').default
    },

    {
        path: '/cart',
        component: require('./components/frontpage/CartComponent.vue').default
    },

    {
        path: '/admin-products',
        component: require('./components/ListProducts').default
    },

    {
        path: '/admin-categories',
        component: require('./components/ListCategories.vue').default
    },

    {
        path: '/admin-orders',
        component: require('./components/ListOrders').default
    },

    {
        path: '/admin/homepage',
        component: require('./components/AdminPage').default
    },
    {
        path: '/admin-categories-create',
        component: require('./components/AddCategory').default
    },

]

const router = new VueRouter({
    routes: routes,
    mode: 'history'
})

const app = new Vue({
    el: '#app',
    store: store,
    router: router
});
