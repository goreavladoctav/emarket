import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';

import VueRouter from 'vue-router'

Vue.use(Vuex);

export default new Vuex.Store({

  state: {
    product: {
      name: "",
      price: 0,
      stock: 0,
      id: 0
    },
    products: [],
    category: {
      name: "",
      id: 0
    },
    categories: [],
    editCategoryForm: false,
    editProductForm: false,
    cart: [],
    orders: [],
    order: {
      id: 0,
      total: 0,
    },
    countCartItems: 0

  },

  getters: {
    product: function(state) {
      return state.product
    },
    products: function(state){
      return state.products
    },
    category: function(state) {
      return state.category
    },
    categories: function(state) {
      return state.categories
    },
    editCategoryForm: function(state) {
      return state.editCategoryForm
    },
    editProductForm: function(state) {
      return state.editProductForm
    },
    cart: function(state) {
      return state.cart
    },
    orders: function(state) {
      return state.orders
    },
    order: function(state) {
      return state.order
    },
    countCartItems: function(state) {
      return state.countCartItems
    }


  },

  mutations: {
    PRODUCT: function(state, product) {
      state.product = product
    },
    PRODUCTS: function(state, products) {
      state.products = products
    },
    CATEGORY: function(state, category) {
      state.category = category
    },
    CATEGORIES: function(state, categories) {
      state.categories = categories
    },
    EDITCATEGORYFORM: function(state, editCategoryForm) {
      state.editCategoryForm = editCategoryForm;
    },
    EDITPRODUCTFORM: function(state, editProductForm) {
      state.editProductForm = editProductForm; 
    },
    CART: function(state, cart) {
      console.log(cart)
      state.cart = cart 
      localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    ADDTO_CART: function(state, product) { //adauga un produs in cos.
      let existProduct = false; //verificam daca exista produsul
      let existProductIndex = -1;
      for(let i = 0; i<state.cart.length; i++) {
        if(product.id  == state.cart[i].id) {
          existProduct = true;
          existProductIndex = i
          state.cart[i].quantity += 1 ;
          state.cart = state.cart
        }
      }
      if(!existProduct) { //daca nu exista se adauga.

        product.quantity = 1

        state.cart.push(product)
      }

      let cart = localStorage.getItem('cart')

      if (cart) {
        let cartObject = JSON.parse(cart)
        if(existProduct) {
          cartObject[existProductIndex].quantity += 1
        }
        else {
          cartObject.push(product)
        }
        
        localStorage.setItem('cart', JSON.stringify(cartObject))
      }
      else {
        localStorage.setItem('cart', JSON.stringify(state.cart))
      }
      
      state.countCartItems = state.cart.length
     
      
      
    },

    DELETEFROM_CART: function(state, product) {
      for(let i = 0 ; i<state.cart.length; i++) {
        if(product.id == state.cart[i].id) {
          state.cart.splice(i, 1)
        }
      } 
      localStorage.setItem('cart', JSON.stringify(state.cart))

      state.countCartItems = state.cart.length          

    },
    ORDERS: function(state, orders) {
      state.orders = orders
    },

    ORDER: function(state, order) {
      state.order = order
    },
    COUNTCARTITEMS: function(state, countCartItems) {
      state.countCartItems = countCartItems
    }

  },

  actions: {

    createProduct: function (store, product) {
      let formData = new FormData

      formData.append('image', product.image)
      formData.append('name', product.name)
      formData.append('price', product.price)
      formData.append('stock', product.stock)
      formData.append('category_id', product.category_id)
     
      axios.post('http://192.168.10.10:8000/admin/products', formData)
        .then( () =>{
          store.dispatch('listProducts')
        })
    },

    listProducts: function(store) {
      axios.get('http://192.168.10.10:8000/frontproducts')
        .then((response) => {
          store.commit('PRODUCTS', response.data)
        })
    },

    deleteProduct: function(store, id) {
      axios.delete('http://192.168.10.10:8000/admin/products/' + id)
        .then(()=> {
          store.dispatch('listProducts');
        })
    },

    editProduct: function(store, id) {
      axios.get('http://192.168.10.10:8000/admin/products/' + id)
        .then(response => {
          store.commit('PRODUCT', response.data)
        })
    },

    updateProduct: function(store, product){
      axios.put('http://192.168.10.10:8000/admin/products/' + product.id, product)
        .then(() => {
          store.dispatch('listProducts')
        })  
    },

    createCategory: function(store, category) {
      axios.post('http://192.168.10.10:8000/admin/categories', category)
        .then(()=>{
          //store.dispatch('listCategories')
          VueRouter.push('/admin-categories')
        })
    },
    listCategories: function(store) {
      axios.get('http://192.168.10.10:8000/admin/categories')
        .then(response =>{
          store.commit("CATEGORIES", response.data)
        })
    },
    editCategory: function(store, id) {
      axios.get('http://192.168.10.10:8000/admin/categories/' + id)
        .then(response=>{
          store.commit('CATEGORY', response.data)
        })
    },
    updateCategory: function(store, category) {
      axios.put('http://192.168.10.10:8000/admin/categories/' + category.id , category)
        .then(()=>{
          store.dispatch('listCategories');
        })
    },
    deleteCategory: function(store, id) {
      axios.delete('http://192.168.10.10:8000/admin/categories/' + id)
        .then(()=> {
          store.dispatch('listCategories');
        })
    },

    checkout: function(store) {
      axios.post('http://192.168.10.10:8000/checkout', {cart: store.getters.cart }) 
        .then(() => {
          store.commit('CART', [])
          localStorage.setItem('cart', JSON.stringify([]))
          store.commit('COUNTCARTITEMS', 0)
        })
    },

    myOrders: function(store) {
      axios.get('http://192.168.10.10:8000/orders')
        .then( response => {
          store.commit('ORDERS', response.data)
        })
    },

    adminOrders: function(store) {
      axios.get('http://192.168.10.10:8000/admin/orders')
        .then( response => {
          store.commit('ORDERS', response.data)
        })
    },
    reset: function(store) {
      store.commit('PRODUCT', {
        name: "",
        price: 0,
        stock: 0,
        id: 0
      })
    },
    resetOrder: function(store) {
      store.commit('ORDER', {
        id: 0,
        total: 0,
      })
    },
    getProductById: function(store, id) {
      axios.get('http://192.168.10.10:8000/admin/products/' + id) 
        .then(response => {
          store.commit('PRODUCT', response.data)
        })
    },
    getOrderById: function(store, id) {
      axios.get('http://192.168.10.10:8000/admin/orders/' + id)
        .then(response => {
          store.commit('ORDER', response.data)
        })
    },
    getCart: function(store) {
      let cart = []
      if(localStorage.getItem('cart') !== null){
        cart = JSON.parse(localStorage.getItem('cart'))
      }
      store.commit('CART', cart )
      store.commit('COUNTCARTITEMS', cart.length)

    }
  }
  
})