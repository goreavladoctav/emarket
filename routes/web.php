<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\FrontPageController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController; // am dat alias

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [FrontPageController::class, 'index']);

Route::post('/checkout', [CheckoutController::class, 'checkout']);

Route::resource('orders', OrderController::class);

Route::prefix('admin')->middleware(['admin.user'])->group(function () {
    // Route::get('/homepage', function(){
    //     return view('products.index');
    // });
    Route::resource('products', AdminProductController::class );// am definit rutele pentru admin.
    Route::resource('categories', CategoryController::class);
    Route::resource('orders', AdminOrderController::class);
});

Route::resource('frontproducts', ProductController::class);

Route::get('{path}', function() {
    return view('layouts.app');
  })->where('path', '.*');




